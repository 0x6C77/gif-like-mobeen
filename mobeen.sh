#!/bin/bash

regex='^[yY]ou should ([^?!.]*)'
ffmpegOptions='-hide_banner -loglevel panic -y'
ffmpegText='fontcolor=white: fontsize=24: shadowcolor=black: shadowx=2:shadowy=2: x=(w-text_w)/2: y=h-text_h-20'

if [[ $1 =~ $regex ]]; then
    subject=${BASH_REMATCH[1]}
else
    echo "Input does not match expected pattern"
    exit 1
fi

text1="You should $subject."
text2="I don’t want to $subject."

echo "Generating GIF for '$subject'"


echo "Creating gif..."
ffmpeg $ffmpegOptions -i clip.mp4 -vf fps=10,scale=720:-1:flags=lanczos,crop=720:305:0:50,palettegen palette.png
ffmpeg $ffmpegOptions -i clip.mp4 -i palette.png -filter_complex "fps=10,scale=720:-1:flags=lanczos[x];[x][1:v]paletteuse,crop=720:305:0:50" temp.gif


echo "Adding text..."
ffmpeg $ffmpegOptions -i temp.gif -vf \
"drawtext=text='$text1':enable='between(t,0.5,2): $ffmpegText', \
drawtext=text='$text2':enable='between(t,2.2,4.6): $ffmpegText'" \
clip.gif


echo "Removing temporary files..."
rm palette.png
rm temp.gif


echo "GIF created at `pwd`/clip.gif"
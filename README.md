# GIF like Mobeen

A GIF generator for the best scene in [Man like Mobeen](https://www.imdb.com/title/tt7639280/).

Clip taken from [Series 1: 3. Upper Room](https://www.bbc.co.uk/iplayer/episode/p05pgmyl/man-like-mobeen-series-1-3-upper-room). Timestamp: 05:44 - 05:49.

Video file not included in repository for legal reasons. You may create your own and save it in the root of the project as `original.mp4` before running commands.

```bash
ffmpeg -y -i original.mp4 -ss 00:05:44 -to 00:05:49.700 -c copy clip.mp4
```

## Usage
```bash
./mobeen.sh "You should get a car"
```

Output:

![](clip.gif)